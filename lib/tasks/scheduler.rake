desc "This task is called by the Heroku scheduler add-on"
task :check_new_tweets => :environment do
  puts "Updating feed..."
  MainController.checkNewTweets
  MainController.cleanOldPosts(Tweeet)
  puts "done."
end

desc "This task is called by the Heroku scheduler add-on 2"
task :check_new_vk_posts => :environment do
  puts "Updating vk post..."
  MainController.checkNewVKPosts
  MainController.cleanOldPosts(VkPost)
  puts "done."
end

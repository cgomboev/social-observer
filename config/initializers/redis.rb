uri = URI.parse(ENV["REDISTOGO_URL"]) rescue nil
REDIS = uri ? Redis.new(:url => ENV['REDISTOGO_URL']) : Redis.new
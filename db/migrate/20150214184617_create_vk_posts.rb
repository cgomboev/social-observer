class CreateVkPosts < ActiveRecord::Migration
  def change
    create_table :vk_posts do |t|
      t.string :post_id
      t.text :post_url
      t.text :full_text

      t.timestamps null: false
    end
  end
end

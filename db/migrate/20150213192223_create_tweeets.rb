class CreateTweeets < ActiveRecord::Migration
  def change
    create_table :tweeets do |t|
      t.string :tweet_id
      t.text :tweet_url
      t.text :full_text

      t.timestamps null: false
    end
  end
end

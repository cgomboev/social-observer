class CreateSlackCounts < ActiveRecord::Migration
  def change
    create_table :slack_counts do |t|
      t.integer :count

      t.timestamps null: false
    end
  end
end

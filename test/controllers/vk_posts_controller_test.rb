require 'test_helper'

class VkPostsControllerTest < ActionController::TestCase
  setup do
    @vk_post = vk_posts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vk_posts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vk_post" do
    assert_difference('VkPost.count') do
      post :create, vk_post: { full_text: @vk_post.full_text, post_id: @vk_post.post_id, post_url: @vk_post.post_url }
    end

    assert_redirected_to vk_post_path(assigns(:vk_post))
  end

  test "should show vk_post" do
    get :show, id: @vk_post
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vk_post
    assert_response :success
  end

  test "should update vk_post" do
    patch :update, id: @vk_post, vk_post: { full_text: @vk_post.full_text, post_id: @vk_post.post_id, post_url: @vk_post.post_url }
    assert_redirected_to vk_post_path(assigns(:vk_post))
  end

  test "should destroy vk_post" do
    assert_difference('VkPost.count', -1) do
      delete :destroy, id: @vk_post
    end

    assert_redirected_to vk_posts_path
  end
end

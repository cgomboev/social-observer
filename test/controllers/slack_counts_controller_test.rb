require 'test_helper'

class SlackCountsControllerTest < ActionController::TestCase
  setup do
    @slack_count = slack_counts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:slack_counts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create slack_count" do
    assert_difference('SlackCount.count') do
      post :create, slack_count: { count: @slack_count.count }
    end

    assert_redirected_to slack_count_path(assigns(:slack_count))
  end

  test "should show slack_count" do
    get :show, id: @slack_count
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @slack_count
    assert_response :success
  end

  test "should update slack_count" do
    patch :update, id: @slack_count, slack_count: { count: @slack_count.count }
    assert_redirected_to slack_count_path(assigns(:slack_count))
  end

  test "should destroy slack_count" do
    assert_difference('SlackCount.count', -1) do
      delete :destroy, id: @slack_count
    end

    assert_redirected_to slack_counts_path
  end
end

require 'test_helper'

class MainControllerTest < ActionController::TestCase
  test "Erase old tweeets" do
    15.times { |i| Tweeet.create! }
    last_id = Tweeet.pluck(:id).sort.last
    MainController.cleanOldPosts(Tweeet, 10)
    assert_equal(Tweeet.count, 10)
    assert(Tweeet.find(last_id).present?)
  end

  test "Erase old vk_posts" do
    15.times { |i| VkPost.create! }
    last_id = VkPost.pluck(:id).sort.last
    MainController.cleanOldPosts(VkPost, 5)
    assert_equal(VkPost.count, 5)
    assert(VkPost.find(last_id).present?)
  end
end

require 'net/http'
require 'uri'
require 'httparty'
require 'twitter'
require 'vkontakte_api'

class MainController < ApplicationController

	def index

        VkPost.destroy_all
    	MainController.checkNewVKPosts #checkNewVKPosts


	end

	def self.cleanOldPosts(klass, limit=4500)
		old_post = klass.order("id desc").offset(limit).first
		if old_post
			klass.where("id <= ?", old_post.id).delete_all
		end
	end

	def self.checkNewTweets


    	client = Twitter::REST::Client.new do |config|
	  		config.consumer_key        = "WljZVuQUBKSV4V1DPs1YuQrmR"
	  		config.consumer_secret     = "K4mk6Mup6tkpENtoU0b9ib1JXpicYK6wQGl7DkfrkMphjTmg9r"
	  		config.access_token        = "283269659-iqXOQAkYjcU8jdWwvJ2F3ybQEUqmaAQUTf7oZw1U"
	  		config.access_token_secret = "O86fFPdjnxYupSsQl8455cnngUXG8SRcNcsT75FF8WnWX"
		end

		new_tweets = Array[]

		client.search("bookmate OR букмейт AND -“bookmate-net“", result_type: "mixed").take(100).collect do |tweet|
			if(tweet.retweet?)
				next
			end
			tweeet = Tweeet.find_by_tweet_id(tweet.id.to_s)
			if(tweeet)

				puts "old tweet #{tweet.text}"
			else
            logger.info "creating"

				tweeet = Tweeet.new({:tweet_id => tweet.id.to_s,
									 :tweet_url => tweet.uri,
									 :full_text => tweet.full_text})
				if (tweeet.save)
		          new_tweets.push(tweeet)
		              logger.info "created"

		        else
		              logger.info "fiRTS"
		        end
		    end
		end


		new_tweets.reverse_each do |tweeet|
			MainController.sendTweetToSlack(tweeet)
			sleep(2.0)
		end

	end

	def self.sendTweetToSlack(tweeet)

		params = {
  	    		  "username" => "Bookmate Twitter Watcher",
	              "text" => "New Tweet. <#{tweeet.tweet_url} | Link>",
	              "icon_emoji"=> ":tw_icon:",
	              "unfurl_links" => true
	              }

	    self.postToSlack(params)

	end







	def self.checkNewVKPosts

		vk = VkontakteApi::Client.new

		new_posts = Array[]
		is_count = true
		vk.newsfeed.search(q: "bookmate", count: 50, extended: 1, fields: ["photo_200","screen_name"] ) do |newsfeed|

			#первый елемент в списке ебучий каунт самого списка
			if(is_count)
				is_count = false
				next
			end

			self.handleNewsfeed(newsfeed,new_posts,is_count)
		end

    	is_count = true
		vk.newsfeed.search(q: "букмейт", count: 50, extended: 1, fields: ["photo_200","screen_name"] ) do |newsfeed|

			#первый елемент в списке ебучий каунт самого списка
			if(is_count)
				is_count = false
				next
			end

			self.handleNewsfeed(newsfeed,new_posts,is_count)
		end

		new_posts.reverse_each do |newsfeed|
			self.sendVKPostToSlack(newsfeed)
			sleep(2.0)
		end

	end

	def self.handleNewsfeed(newsfeed, new_posts, is_count)

		#отфильтровать от постов созданых их приложение, определяем по строке \" on Bookmate, » на Букмейт
		if (newsfeed.text.include? "\" on Bookmate") || (newsfeed.text.include? "» на Букмейт")
			puts "SKIP POST #{newsfeed.text}"
				return
		end



		post = VkPost.find_by_post_id(newsfeed.id.to_s)
		if(post)

			puts "old vk_post #{post.post_id}"
		else
        	logger.info "creating"

			post = VkPost.new({
				:post_id => newsfeed.id.to_s,
				    :post_url => "https://vk.com/feed?w=wall#{newsfeed.owner_id}_#{newsfeed.id}",
				:full_text => newsfeed.text
			})

			if (post.save)
	          new_posts.push(newsfeed)
	              logger.info "created #{newsfeed.id}"
	        else
	              logger.info "fail"
	        end
	    end
	end

	def self.sendVKPostToSlack (newsfeed)


    	attachments = Array[]


		attachments.push ( {
		    "fallback" => "Required plain-text summary of the attachment.",

		    #"color" => "#36a64f",

		    # "pretext" => "Optional text that appears above the attachment block",

		    "author_name" => newsfeed.user ? "#{newsfeed.user.first_name} #{newsfeed.user.last_name} @#{newsfeed.user.screen_name}" : "#{newsfeed.group.name} @#{newsfeed.group.screen_name}",
		    "author_link" => newsfeed.user ? "https://vk.com/#{newsfeed.user.screen_name}" : "https://vk.com/#{newsfeed.group.screen_name}",
		    "author_icon" => newsfeed.user ? "#{newsfeed.user.photo ? newsfeed.user.photo : newsfeed.user.photo_200}" : "#{newsfeed.group.photo ? newsfeed.group.photo : newsfeed.group.photo_200}",

		    "text" => newsfeed.text,

		})

		if(newsfeed.attachments)
			if(newsfeed.attachments.count == 1 && newsfeed.attachment.type.eql?("photo"))

				attachment = attachments.first

				temp_attachment = self.getDictFromAttachment(newsfeed.attachment,newsfeed.id)

				if(temp_attachment)

					temp_attachment.delete("fallback")

					attachment.merge(temp_attachment)
				end

			else
				newsfeed.attachments.each do |attachment|

					temp_attachment = self.getDictFromAttachment(attachment,newsfeed.id)

					if(temp_attachment)
						attachments.push(temp_attachment)
					end

				end
			end
		end

	    post_url = "https://vk.com/feed?w=wall#{newsfeed.owner_id}_#{newsfeed.id}"

    	params = {
    		"username" => "Bookmate VK Watcher",
            "text" => " New VK POST. <#{post_url} | Link>",
            "icon_emoji" => ":vk_icon:",
            "unfurl_links" => true,
            # "channel" => "#test-aki",
            "attachments" => attachments
        }

	    self.postToSlack(params)

 	end

  	def self.getDictFromAttachment(attachment,newsfeed_id)

  		if(attachment.type.eql? "photo")

			resultAttachment = {
    			"fallback" => "#{newsfeed_id} photo #{attachment.photo.src_big}",
		    	"image_url" => attachment.photo.src_big
			}
		end

		if(attachment.type.eql? "link")

			resultAttachment = {
				"fallback" => "#{newsfeed_id} link #{attachment.link.url}",
				"title" => attachment.link.title,
			    "title_link" => attachment.link.url,
		    	"text" => attachment.link.description,
				"image_url" => attachment.link.image_src
			}
		end

		return resultAttachment
  	end

  	def self.postToSlack(params)

  		url = ""
  		self.find_count
  		slack_count = SlackCount.all.first
  		if slack_count.count % 2 == 0
  			url = 'https://hooks.slack.com/services/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'
  			puts "sending by first"
  		else
	        url = 'https://hooks.slack.com/services/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'
  			puts "sending by second"
		end

		slack_count.count += 1
	    response_body = HTTParty.post( url, { :body => { :payload => params.to_json }}).body
	    puts "RESPONSE #{response_body}"
		if(slack_count == 100)
			slack_count == 0
		end
		slack_count.save

  	end

	def self.find_count

		count = SlackCount.all
		puts "COUNT #{count}"
		if(!count)
			obj = SlackCount.new({:count => 0})
			obj.save
			puts "NEWCOUNT"
		else
			obj = count.first
			puts "FINDED ONE"

		end
		#return obj

	end

end


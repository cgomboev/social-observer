class SlackCountsController < ApplicationController
  before_action :set_slack_count, only: [:show, :edit, :update, :destroy]

  # GET /slack_counts
  # GET /slack_counts.json
  def index
    @slack_counts = SlackCount.all
  end

  # GET /slack_counts/1
  # GET /slack_counts/1.json
  def show
  end

  # GET /slack_counts/new
  def new
    @slack_count = SlackCount.new
  end

  # GET /slack_counts/1/edit
  def edit
  end

  # POST /slack_counts
  # POST /slack_counts.json
  def create
    @slack_count = SlackCount.new(slack_count_params)

    respond_to do |format|
      if @slack_count.save
        format.html { redirect_to @slack_count, notice: 'Slack count was successfully created.' }
        format.json { render :show, status: :created, location: @slack_count }
      else
        format.html { render :new }
        format.json { render json: @slack_count.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /slack_counts/1
  # PATCH/PUT /slack_counts/1.json
  def update
    respond_to do |format|
      if @slack_count.update(slack_count_params)
        format.html { redirect_to @slack_count, notice: 'Slack count was successfully updated.' }
        format.json { render :show, status: :ok, location: @slack_count }
      else
        format.html { render :edit }
        format.json { render json: @slack_count.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /slack_counts/1
  # DELETE /slack_counts/1.json
  def destroy
    @slack_count.destroy
    respond_to do |format|
      format.html { redirect_to slack_counts_url, notice: 'Slack count was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_slack_count
      @slack_count = SlackCount.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def slack_count_params
      params.require(:slack_count).permit(:count)
    end
end

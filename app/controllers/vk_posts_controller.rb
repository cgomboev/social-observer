class VkPostsController < ApplicationController
  before_action :set_vk_post, only: [:show, :edit, :update, :destroy]

  # GET /vk_posts
  # GET /vk_posts.json
  def index
    @vk_posts = VkPost.all
  end

  # GET /vk_posts/1
  # GET /vk_posts/1.json
  def show
  end

  # GET /vk_posts/new
  def new
    @vk_post = VkPost.new
  end

  # GET /vk_posts/1/edit
  def edit
  end

  # POST /vk_posts
  # POST /vk_posts.json
  def create
    @vk_post = VkPost.new(vk_post_params)

    respond_to do |format|
      if @vk_post.save
        format.html { redirect_to @vk_post, notice: 'Vk post was successfully created.' }
        format.json { render :show, status: :created, location: @vk_post }
      else
        format.html { render :new }
        format.json { render json: @vk_post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vk_posts/1
  # PATCH/PUT /vk_posts/1.json
  def update
    respond_to do |format|
      if @vk_post.update(vk_post_params)
        format.html { redirect_to @vk_post, notice: 'Vk post was successfully updated.' }
        format.json { render :show, status: :ok, location: @vk_post }
      else
        format.html { render :edit }
        format.json { render json: @vk_post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vk_posts/1
  # DELETE /vk_posts/1.json
  def destroy
    @vk_post.destroy
    respond_to do |format|
      format.html { redirect_to vk_posts_url, notice: 'Vk post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vk_post
      @vk_post = VkPost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vk_post_params
      params.require(:vk_post).permit(:post_id, :post_url, :full_text)
    end
end

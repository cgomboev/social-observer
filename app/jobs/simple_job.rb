class SimpleJob
  include Sidekiq::Worker

  def perform(name, count)
    puts "Job is begin"

    MainController.checkNewTweets
    
    puts "Job is done"
    
    SimpleJob.perform_in(10.seconds, 'bob', 5)

  end
end

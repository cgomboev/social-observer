json.array!(@vk_posts) do |vk_post|
  json.extract! vk_post, :id, :post_id, :post_url, :full_text
  json.url vk_post_url(vk_post, format: :json)
end

json.array!(@tweeets) do |tweeet|
  json.extract! tweeet, :id, :tweet_id, :tweet_url, :full_text
  json.url tweeet_url(tweeet, format: :json)
end

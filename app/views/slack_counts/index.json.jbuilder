json.array!(@slack_counts) do |slack_count|
  json.extract! slack_count, :id, :count
  json.url slack_count_url(slack_count, format: :json)
end
